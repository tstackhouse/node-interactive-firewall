const uuid = require('uuid/v1');

const addWhitelist = ip => ({
  type: 'ADD_WHITELIST',
  id: uuid(),
  ip
});

const addRecent = (ip, port) => ({
  type: 'ADD_RECENT',
  id: uuid(),
  ip,
  port
});

module.exports = {
  addWhitelist,
  addRecent
};
