const token = process.env.BOT_TOKEN;
// const chatId = process.env.CHAT_ID;

const TelegramBot = require('node-telegram-bot-api');

class Bot {
  constructor(store) {
    if (! store) {
      throw new TypeError('Store is required for Bot!');
    }

    this.store = store;
    this.bot =  new TelegramBot(token, {polling: true});

    // this.bot.on('message', (...args) => this.onMessage(...args));
    this.bot.onText(/\/recent/, (...args) => this.onRecent(...args));
  }

  onMessage(msg) {
    const chatId = msg.chat.id;

    console.log(msg);
  
    // send a message to the chat acknowledging receipt of their message
    bot.sendMessage(chatId, 'Received your swag');
  }

  onRecent(msg) {
    const chatId = msg.chat.id;
    const recents = this.store.getState().recents;
    
    if (recents.length > 0) {
      this.bot.sendMessage(chatId, `Here are the last ${recents.length} IPs I've seen:`);
      recents.forEach(recent => {
        this.bot.sendMessage(chatId, recent.ip);
      });
      return;
    }

    this.bot.sendMessage(chatId, 'I haven\'t seen any connections lately.');
  }

  sendMessage(chatId, msg) {
    this.bot.sendMessage(chatId, msg);    
  }
}

module.exports = Bot;