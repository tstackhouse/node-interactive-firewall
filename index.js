const { createStore } = require('redux');

const rootReducer = require('./reducers');
const {
  addWhitelist,
  addRecent
} = require('./actions');

const Proxy = require('./proxy');
const Bot = require('./bot');
const Whitelist = require('./whitelist');

const chatId = process.env.CHAT_ID;

const store = createStore(rootReducer);
store.subscribe((...args) => {
  console.log(...args);
  console.log(store.getState())
});

const bot = new Bot(store);
const whitelist = new Whitelist(store);

store.dispatch(addWhitelist('::1'));

const handleRequest = (c) => {
  store.dispatch(addRecent(c.remoteAddress, c.localPort));

  return whitelist.checkIp(c.remoteAddress);
};

new Proxy('12345', '4200', handleRequest);
