// Adapted from https://stackoverflow.com/a/19637388/2823783

const net = require('net');

// parse "80" and "localhost:80" or even "42mEANINg-life.com:80"
const addrRegex = /^(([a-zA-Z\-\.0-9]+):)?(\d+)$/;

class Proxy {
  constructor(from, to, allow) {
    if (! from || ! to) {
      throw new TypeError('From/To Addresses required');
    }

    this.allow = () => true;
    if (allow && typeof allow === 'function') {
      this.allow = allow;
    }
    
    this.addr = {
      from: addrRegex.exec(from),
      to: addrRegex.exec(to)
    };

    console.log(`New forwarder from ${this.addr.from[2]}:${this.addr.from[3]} to ${this.addr.to[2]}:${this.addr.to[3]}`);

    net.createServer((c) => this.handleConnection(c)).listen(this.addr.from[3], this.addr.from[2]);
  }

  async handleConnection(fromConn) {
    // console.log(`New connection from ${fromConn.remoteAddress}:${fromConn.remotePort} on ${fromConn.localAddress}:${fromConn.localPort}`);
    if (await this.allow(fromConn)) {
      // console.log(`Connection Allowed`);

      const to = net.createConnection({
        host: this.addr.to[2],
        port: this.addr.to[3]
      }); // TODO: Handle connection failures

      fromConn.pipe(to);
      to.pipe(fromConn);
      return;
    }

    // console.log(`Connection Denied`);
    fromConn.destroy();
  }
}

module.exports = Proxy;