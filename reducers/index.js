const { combineReducers } = require('redux');
const whitelist = require('./whitelist');
const recents = require('./recents');

module.exports = combineReducers({
  whitelist,
  recents
});