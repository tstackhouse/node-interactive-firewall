const recents = (state = [], action) => {
  switch (action.type) {
    case 'ADD_RECENT':
      if (! state.find((r) => r.ip === action.ip)) {
        return [
          ...state.slice(1, 5),
          {
            id: action.id,
            ip: action.ip,
            port: action.port
          }
        ];
      }
      return state;      
    default:
      return state;
  }
}

module.exports = recents;
