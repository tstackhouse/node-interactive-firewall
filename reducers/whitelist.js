const whitelist = (state = [], action) => {
  switch (action.type) {
    case 'ADD_WHITELIST':
      return [
        ...state,
        {
          id: action.id,
          ip: action.ip
        }
      ];
    default:
      return state;
  }
};

module.exports = whitelist;
