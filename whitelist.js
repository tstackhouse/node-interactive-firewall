class Whitelist {
  constructor(store) {
    if (! store) {
      throw new TypeError('Whitelist requires store!');
    }

    this.store = store;
  }

  checkIp(ip) {
    return !! this.store.getState().whitelist.find((w) => w.ip === ip);
  }
}

module.exports = Whitelist;
